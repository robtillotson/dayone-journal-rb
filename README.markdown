# dayone-journal-rb

Day One (http://dayoneapp.com/) is a daily journal application for Mac OS X and
iOS.  Because it uses Dropbox for syncing, Markdown for formatting, and stores
its entries in XML property lists with plain text for the main content, it is
amenable to use with external applications.  Thus this library, which provides
a Ruby interface to manipulate a Day One journal package.


## Requirements

This library is developed with Ruby 1.9.2.  It may or may not work with other
versions.

The following gems are required (install Bundler and run "bundle install" to
install them automatically):

- plist4r
- simple_uuid


## Usage

    require 'dayone-journal'

    journal = DayOne::Journal.new  # Pass optional path if not ~/Dropbox/Journal.dayone
    journal.each  # Iterate through entries.  Journal is Enumerable including sorting.
    entry = journal.new_entry  # Create new entry in journal.  Can pass hash args and/or a block.
    entry.uuid, entry.text, entry.date, entry.starred  # Attributes of the entry
    entry.save  # Save the entry

That's all for now.  More to come.

 
## Copyright

Copyright 2011 Rob Tillotson. See LICENSE.txt for further details.

