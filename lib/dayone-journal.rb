
require 'plist4r'
require 'plist4r/plist_type'

require 'simple_uuid'

Plist4r::Config[:strict_keys] = true

module Plist4r
  class PlistType::DayOneEntry < PlistType
    ValidKeys = {
      :string => [ "UUID", "Entry Text" ],
      :bool => [ "Starred" ],
      :datetime => [ "Creation Date" ]
    }

    def date(value=nil)
      key = "Creation Date"

      case value
      when Time, Date, DateTime
        @hash[key] = value
      when nil
        @hash[key]
      else
        raise "Invalid value: #{method_name} #{value.inspect}. Should be Date, Time, or DateTime."
      end
    end

    def text(value=nil)
      key = "Entry Text"

      case value
      when String
        @hash[key] = value
      when nil
        @hash[key]
      else
        raise "Invalid value: #{method_name} #{value.inspect}. Should be string."
      end
    end

    def uuid(value=nil)
      key = "UUID"

      case value
      when String
        @hash[key] = value
      when nil
        @hash[key]
      else
        raise "Invalid value: #{method_name} #{value.inspect}. Should be string."
      end
    end
  end
end

Plist4r::DataMethods::ClassesForKeyType[:datetime] = [Time, Date, DateTime]
Plist4r::Config[:types] << "day_one_entry"

module DayOne
  class Entry
    include Comparable

    # This is quicker than rolling 4+ accessors by hand.
    def self.plist_accessor(*names)
      names.each do |name|
        define_method name do
          @plist.send(name)
        end

        define_method "#{name}=" do |v|
          @plist.send(name, v)
        end
      end
    end

    # day one plist contents
    plist_accessor :uuid, :text, :date, :starred
    # general plist attributes
    plist_accessor :filename, :filename_path

    attr_reader :journal, :plist

    def initialize(journal, plist=nil)
      @journal = journal
      if plist.nil?
        uuid = journal.generate_uuid
        @plist = Plist4r::Plist.new
        @plist.plist_type :day_one_entry
        @plist.path journal.entries_path
        @plist.filename uuid + ".doentry"
        @plist.file_format :xml

        @plist.uuid uuid
        @plist.date Time.now
        @plist.text ""
        @plist.starred false
      else
        @plist = plist
      end
    end

    def save(filename=nil)
      if filename.nil?
        @plist.save
      else
        @plist.save_as filename
      end
    end

    def <=>(other)
      case other.class
      when Entry
        self.date <=> other.date
      when Date, Time, DateTime
        self.date <=> other
      when String
        self.uuid <=> other
      else
        raise ArgumentError, "invalid argument type for comparison"
      end
    end
  end

  class Journal
    include Enumerable

    attr_accessor :path

    def initialize(path=nil)
      path = "~/Dropbox/Journal.dayone" if path.nil?
      @path = File.expand_path(path)
    end

    def entries_path
      File.join(path,"entries")
    end

    def generate_uuid
      SimpleUUID::UUID.new.to_guid.upcase.gsub(/\-/,"")
    end

    def each
      Dir.glob(File.join(entries_path, "*.doentry")) do |filename|
        yield Entry.new(self, Plist4r.open(filename))
      end
    end

    def new_entry(args={})
      x = Entry.new(self)
      args.each do |k,v|
        x.send("#{k}=".to_sym, v)
      end

      if block_given?
        yield x
      end
      x
    end

  end
end

